Project Name: 
DemoAPIProject

Description: 
This project shows demo of fetching user’s data via API, by configuring end points URL.


Installation: 
Install Postman tool- configure below end points URL to see the results:
1. https://reqres.in/api/users --> List of users are displayed when request is sent in postman tool by using this URL. This depicts for Positive scenario

2. https://reqres.in/api/users?id=9900  this url is used to fetch user details whose user id is-9900 When user sends request to this URL-Postman tool shows user not available. This depicts for Negative scenario

Install the IntelliJ- add all required cucumber JAR files/ Rest assured libraries/Java-JVM Jar & Library files.
Create Project & write Java classes to cover the functionality of user details fetch via API.
This project is developed using Cucumber – BDD Framework.

Once project is ready on IntelliJ on local system.
Create GitLab Account & then Create Project. Make sure to Project name is same as Local copy.
Push the Local Project to GitLab using IntelliJ commands like – Commit/Push

How to Clone this Project on your systems:
Go to GitLab  Click on Project Name 
Click on Clone - Click on ‘Clone with HTTPS’ copy URL icon
On Local select folder where you would like to clone this Project  Open Git Clone  Paste this URL 
& then click on submit button
Project gets installed on your system.

Usage: 
This project shows demo when User tries to fetch data via API.
Below screenshot is reference output generated when user details are fetched.
Covering both Positive & Negative scenario


Authors and acknowledgment
Google 
